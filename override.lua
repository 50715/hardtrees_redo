-- hardtrees_redo/override.lua
hardtrees_redo.override = {} -- override global variable

-- [function] override tree nodes
function hardtrees_redo.override.tree(name)
  -- if tree registered, override
  if minetest.registered_nodes[name] then
    local groups = minetest.registered_nodes[name].groups -- get groups
    groups.oddly_breakable_by_hand = 0 -- breakable by hand false
    minetest.override_item(name, {groups = groups}) -- override
  else
    minetest.log("waving", "WARNING: [hardtrees_redo] "..name.." not registered, could not override.")
  end
end

-- [function] override leaf nodes
function hardtrees_redo.override.leaf(name)

  -- if leaf and sapling register, override
  if minetest.registered_nodes[name] then
    minetest.override_item(name, {
      climbable = true,
      walkable = false
    })
  else
    minetest.log("waving", "WARNING: [hardtrees_redo] "..name.." not registered, could not override.")
  end
end

-- [function] override moretrees
function hardtrees_redo.override.moretrees(name)
    hardtrees_redo.override.tree("moretrees:"..name.."_trunk")
    hardtrees_redo.override.leaf("moretrees:"..name.."_leaves")
end

-- [DEFAULT OVERRIDES]
-- default:tree
hardtrees_redo.override.tree("default:tree")
hardtrees_redo.override.leaf("default:leaves")

-- default:jungle
hardtrees_redo.override.tree("default:jungletree")
hardtrees_redo.override.leaf("default:jungleleaves")

-- default:pine
hardtrees_redo.override.tree("default:pine_tree")
hardtrees_redo.override.leaf("default:pine_needles")

-- default:acacia
hardtrees_redo.override.tree("default:acacia_tree")
hardtrees_redo.override.leaf("default:acacia_leaves")

-- default:aspen
hardtrees_redo.override.tree("default:aspen_tree")
hardtrees_redo.override.leaf("default:aspen_leaves")

-- [MORETREES OVERRIDES]
if minetest.get_modpath("moretrees") then
  -- moretrees:apple_tree
  hardtrees_redo.override.moretrees("apple_tree")

  -- moretrees:beech
  hardtrees_redo.override.moretrees("beech")

  -- moretrees:birch
  hardtrees_redo.override.moretrees("birch")

  -- moretrees:cedar
  hardtrees_redo.override.moretrees("cedar")

  -- moretrees:date_palm
  hardtrees_redo.override.moretrees("date_palm")

  -- moretrees:fir
  hardtrees_redo.override.tree("moretrees:fir_trunk")
  hardtrees_redo.override.leaf("moretrees:fir_leaves")
  hardtrees_redo.override.leaf("moretrees:fir_leaves_bright")

  -- moretrees:jungletree
  hardtrees_redo.override.tree("moretrees:jungletree_trunk")
  hardtrees_redo.override.leaf("moretrees:jungletree_leaves_red")
  hardtrees_redo.override.leaf("moretrees:jungletree_leaves_yellow")

  -- moretrees:palm
  hardtrees_redo.override.moretrees("palm")

  -- moretrees:poplar
  hardtrees_redo.override.moretrees("poplar")

  -- moretrees:rubber
  hardtrees_redo.override.moretrees("rubber_tree")

  -- moretrees:sequoia
  hardtrees_redo.override.moretrees("sequoia")

  -- moretrees:spruce
  hardtrees_redo.override.moretrees("spruce")

  -- moretrees:willow
  hardtrees_redo.override.moretrees("willow")

  -- moretrees:oak
  hardtrees_redo.override.moretrees("oak")
end
